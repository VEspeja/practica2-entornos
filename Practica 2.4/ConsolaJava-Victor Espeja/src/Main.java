import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		int opcion=0;
		String respuesta="";
		
	
			do{
			
					
				System.out.println("CALCULADORA");
				System.out.println("------------------");
				System.out.println("1. SUMA");
				System.out.println("2. RESTA");
				System.out.println("3. MULTIPLICACION");
				System.out.println("4. POTENCIA");
				System.out.println("Pulsar opcion....");
				opcion=in.nextInt();
				
				
				
				
				switch(opcion){
				case 1: 
					
					suma();
				
				break;
				
				case 2:	
					resta();
					
				
				break;
				
				case 3:
					multiplicacion();
				
				break;
				
				case 4:
					potencia();
				
					
				break;
				
				default:
					System.out.println("Has introducido mal la opcion");
				}
				in.nextLine();
			System.out.println("Quiere realizar otra operacion??");
			respuesta=in.nextLine();
			
			
			}while(!respuesta.equalsIgnoreCase("no"));
			
			in.close();
			
		
		
	}
		
	private static void potencia() {
		Scanner in= new Scanner(System.in);
		int n1,n2;
		
		System.out.println("Introduce un numero para la base");
		n1=in.nextInt();
		System.out.println("Introduce otro numero para el exponente");
		n2=in.nextInt();
		
		System.out.println("La potencia de "+n1+" con exponente "+n2+" es = "+Math.pow(n1, n2));
		
	}

	private static void multiplicacion() {
		
		Scanner in= new Scanner(System.in);
		int n1,n2;
		System.out.println("Introduce un numero");
		n1=in.nextInt();
		System.out.println("Introduce otro numero para multiplicar");
		n2=in.nextInt();
		
		System.out.println("La multiplicacion de "+n1+" y "+n2 +" es = "+(n1*n2));
	}

	private static void resta() {
		Scanner in= new Scanner(System.in);
		int n1,n2;
		System.out.println("Introduce un numero");
		n1=in.nextInt();
		System.out.println("Introduce otro numero mayor que el anterior");
		n2=in.nextInt();
		
		System.out.println("La resta de "+n1+" y "+n2 +" es = "+(n1-n2));
		
		
	}

	private static void suma() {
		Scanner in= new Scanner(System.in);
		int n1,n2;
		System.out.println("Introduce un numero");
		n1=in.nextInt();
		System.out.println("Introduce otro numero");
		n2=in.nextInt();
		
		System.out.println("La suma de "+n1+" y "+n2 +" es = "+(n1+n2));
		
	}

	
}
