﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_VictorEspeja
{
    public class Class2
    {
        public static Boolean Calcularprimos(int n)
        {
            for (int i = 2; i <= n - 1; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
        public static int calcularFactorial(int n)
        {
            int factorial = 1;
            for (int i = 1; i <= n; i++)
            {
                factorial = factorial * i;
            }
            return factorial;
        }


        public static void dibujarPiramide(int n)
        {

            for (int linea = 1; linea <= n; linea++)
            {
                for (int i = 1; i <= linea; i++)
                {
                    Console.WriteLine(i);
                }
                Console.WriteLine();
            }
        }

        public static void piramideParesInversa(int n)
        {
            for (int valor = n; valor >= 0; valor = valor - 2)
            {
                for (int i = valor; i >= 0; i = i - 2)
                {
                    Console.WriteLine(i);
                }
                Console.WriteLine();

            }
        }

        public static void dibujarCuadrado(int n)
        {
            for (int linea = 1; linea <= n; linea++)
            {
                for (int i = 1; i <= n; i++)
                {

                    if (linea == 1 || linea == n || i == 1 || i == n || i == linea)
                    {
                        Console.WriteLine("*");

                    }
                    else
                    {
                        Console.WriteLine(" ");
                    }

                }
                Console.WriteLine();
            }
        }
    }
}
