﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_VictorEspeja
{
    public class Class1
    {
        public static int suma(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int resta(int n1, int n2)
        {
            if (n1 < n2)
            {
                return n2 - n1;
            }
            else
            {
                return n1 - n2;
            }
        }

        public static int multiplicar(int n1, int n2)
        {
            return n1 * n2;
        }

        public static void invertirCadena(String[] v, String[] vi)
        {

            int j = 0;
            for (int i = v.Length - 1; i >= 0; i--)
            {
                vi[j] = v[i];
                j++;
            }

        }

        public static void visualizarCadena(String[] v)
        {

            for (int i = 0; i < v.Length; i++)
            {

                Console.WriteLine("La posicion " + (i + 1) + " es " + v[i]);
            }

        }
    }
}
