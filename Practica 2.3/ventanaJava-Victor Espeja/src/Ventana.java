import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.SpinnerNumberModel;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;

/**
 * 
 * @author victor espeja lopez
 * @since 13/12/2017
 */
public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setForeground(Color.RED);
		setBackground(Color.RED);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/Imagen/Car-Launcher-Pro-apk-para-Android-124x124.png")));
		setTitle("Registro de Vehiculos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 857, 536);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.GRAY);
		menuBar.setForeground(Color.GRAY);
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setForeground(Color.BLACK);
		mnArchivo.setBackground(Color.ORANGE);
		menuBar.add(mnArchivo);
		
		JMenu mnNuevo = new JMenu("Nuevo");
		mnArchivo.add(mnNuevo);
		
		JMenuItem mntmArchivoDeRegistro = new JMenuItem("Archivo de registro");
		mnNuevo.add(mntmArchivoDeRegistro);
		
		JMenuItem mntmTablaDePrecios = new JMenuItem("Tabla de precios");
		mnNuevo.add(mntmTablaDePrecios);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnArchivo.add(mntmBuscar);
		
		JMenu mnEditar = new JMenu("Editar");
		mnEditar.setForeground(Color.BLACK);
		mnEditar.setBackground(Color.WHITE);
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnEditar.add(mntmCortar);
		
		JMenuItem mntmBorrar = new JMenuItem("Borrar");
		mnEditar.add(mntmBorrar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("About");
		mnAyuda.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(40, 205, 435, 30);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Negro", "Blanco", "Rojo", "Azul", "Gris"}));
		comboBox.setBounds(40, 358, 150, 22);
		contentPane.add(comboBox);
		
		JLabel lblTipoDeVehiculo = new JLabel("Color del Vehiculo :");
		lblTipoDeVehiculo.setBounds(40, 329, 168, 16);
		contentPane.add(lblTipoDeVehiculo);
		
		JSlider slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setToolTipText("");
		slider.setBackground(new Color(224, 255, 255));
		slider.setValue(5);
		slider.setMaximum(1);
		slider.setBounds(553, 322, 200, 26);
		contentPane.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(1, null, 10, 1));
		spinner.setBounds(129, 405, 45, 22);
		contentPane.add(spinner);
		
		JLabel lblCantidad = new JLabel("Cantidad :");
		lblCantidad.setBounds(47, 408, 81, 16);
		contentPane.add(lblCantidad);
		
		JLabel lblCaracteristicas = new JLabel("Nombre vendedor");
		lblCaracteristicas.setBounds(40, 185, 127, 16);
		contentPane.add(lblCaracteristicas);
		
		JButton btnGuardar = new JButton("Enviar Pedido");
		btnGuardar.setBackground(Color.GREEN);
		btnGuardar.setForeground(new Color(0, 0, 0));
		btnGuardar.setBounds(569, 437, 127, 25);
		contentPane.add(btnGuardar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setForeground(new Color(0, 0, 0));
		btnBorrar.setBackground(Color.RED);
		btnBorrar.setBounds(740, 437, 97, 25);
		contentPane.add(btnBorrar);
		
		JRadioButton rdbtnNuevo = new JRadioButton("Coche");
		rdbtnNuevo.setBackground(new Color(224, 255, 255));
		rdbtnNuevo.setBounds(40, 130, 127, 25);
		contentPane.add(rdbtnNuevo);
		
		JRadioButton rdbtnSegundaManoi = new JRadioButton("Moto");
		rdbtnSegundaManoi.setBackground(new Color(224, 255, 255));
		rdbtnSegundaManoi.setBounds(40, 151, 127, 25);
		contentPane.add(rdbtnSegundaManoi);
		
		JLabel lblEstadoDelVehiculo = new JLabel("Tipo de vehiculo");
		lblEstadoDelVehiculo.setBounds(40, 107, 127, 16);
		contentPane.add(lblEstadoDelVehiculo);
		
		JLabel lblPeriodoDeEntrega = new JLabel("Estado del vehiculo");
		lblPeriodoDeEntrega.setBounds(591, 292, 139, 16);
		contentPane.add(lblPeriodoDeEntrega);
		
		JLabel lblNombreComprador = new JLabel("Nombre comprador");
		lblNombreComprador.setBounds(40, 255, 127, 16);
		contentPane.add(lblNombreComprador);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(40, 273, 435, 30);
		contentPane.add(textField_1);
		
		JCheckBox chckbxAceptoCondicionesDe = new JCheckBox("Acepto condiciones de reserva y compra");
		chckbxAceptoCondicionesDe.setBackground(new Color(224, 255, 255));
		chckbxAceptoCondicionesDe.setBounds(569, 383, 268, 25);
		contentPane.add(chckbxAceptoCondicionesDe);
		
		JLabel lblNuevo = new JLabel("Nuevo");
		lblNuevo.setBounds(542, 350, 56, 16);
		contentPane.add(lblNuevo);
		
		JLabel lblSegundaMano = new JLabel("Segunda Mano");
		lblSegundaMano.setBounds(701, 350, 91, 16);
		contentPane.add(lblSegundaMano);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/Imagen/Car-Launcher-Pro-apk-para-Android-124x124.png")));
		lblNewLabel.setBounds(701, 38, 150, 117);
		contentPane.add(lblNewLabel);
		
		JLabel lblMaxUnidades = new JLabel("* Max. 10 unidades");
		lblMaxUnidades.setBounds(187, 411, 132, 16);
		contentPane.add(lblMaxUnidades);
		
		JLabel lblInfo = new JLabel("Info: ");
		lblInfo.setBounds(186, 394, 56, 16);
		contentPane.add(lblInfo);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(26, 13, 103, 30);
		contentPane.add(toolBar);
		
		JButton button = new JButton("");
		toolBar.add(button);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Ventana.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Ventana.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Cut.png")));
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(Ventana.class.getResource("/javax/swing/plaf/metal/icons/ocean/newFolder.gif")));
		toolBar.add(btnNewButton_2);
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{textField, comboBox, lblTipoDeVehiculo, slider, spinner, lblCantidad, lblCaracteristicas, btnGuardar, btnBorrar, rdbtnNuevo, rdbtnSegundaManoi, lblEstadoDelVehiculo, lblPeriodoDeEntrega}));
	}
}
