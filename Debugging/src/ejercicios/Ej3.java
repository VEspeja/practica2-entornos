package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		/*
		 * El error se produce en la l�nea 23, en cadenaLeida.substring(0, posicionCaracter)
		 *  ya que en la acci�n anterior convierte la variable posicionCaracter en -1 ya que 
		 *  no contiene el car�cter dado, el substring da el error cuando es un valor negativo
		 *   o cuando el valor de la cadena es menor a la posici�n solicitada
		 */
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}

