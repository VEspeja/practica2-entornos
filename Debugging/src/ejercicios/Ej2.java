package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		/*
		 * En la l�nea del if se produce el error ya que esta intentando convertir
		 * �cadenaLeida� en un numero pero no se convierte nada ya que no aparece
		 * ning�n car�cter �cadenaLeida=� � (id=24)�, es una variable vac�a ya
		 * que en numeroLeido, se ha guardado el Intro y le ha dado ese valor a
		 * la variable cadenaLeida
		 */
		
		/*
		 * El error es java.lang.NumberFormatException: For input string: ""
		 * Se soluciona poniendo un input.nextLine(); despu�s de numeroLeido= input.nextInt();
		 */
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
